<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet"
	integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<title>ALEXNET</title>
</head>
<style>
.input-group-addon {
	color: #fff;
	background-color: #7f8183;
}

#loginbox {
	left: -15px;
}

#signupbox {
	left: -62px;
}

.form-control, .input-group-addon {
	border-radius: 0px;
}

label {
	text-align: left !important;
}

body {
	background-image: url("../../newBackground.jpg");
}

#demotext {
	color: #FFFFFF;
	text-shadow: 1px 3px 0 #969696, 1px 13px 5px #aba8a8;
	color: #FFFFFF;
}
</style>
<body>
	<div id="demotext">
		<center>
			<h2>Before you continue you need to Login</h2>
		</center>
	</div>
	<div class="container">
		<div class="container">
			<div id="loginbox" style="margin-top: 50px;"
				class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title text-center">
							<i class="fa fa-sign-in" aria-hidden="true"></i> Sign In
						</div>
					</div>
					<div style="padding-top: 30px" class="panel-body">
						<div style="display: none" id="login-alert"
							class="alert alert-danger col-sm-12"></div>
						<form:form id="loginform" class="form-horizontal"
							modelAttribute="userLogin" action="/login?j_spring_security_check" method="POST">
							<div style="margin-bottom: 25px"
								class="input-group col-sm-offset-3 col-sm-7">
								<span class="input-group-addon"><i
									class="glyphicon glyphicon-user"></i></span> <input
									id="login-firstName" type="text" class="form-control input-sm"
									name="email" placeholder="Email">
							</div>
							<div style="margin-bottom: 25px"
								class="input-group col-sm-offset-3 col-sm-7">
								<span class="input-group-addon"><i
									class="glyphicon glyphicon-lock"></i></span> <input
									id="login-password" type="password"
									class="form-control input-sm" name="password"
									placeholder="password">
							</div>
							<div style="margin-top: 10px" class="form-group">
								<div class="col-sm-12 controls text-center">
									<input type="submit" class="btn btn-default btn-sm"
										value="Login">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 control">
									<div
										style="border-top: 1px solid #888; padding-top: 15px; font-size: 85%">
										Don't have an account! <a href="#"
											onClick="$('#loginbox').hide(); $('#signupbox').show()">
											Sign Up Here </a>
									</div>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
			<div id="signupbox" style="display: none; margin-top: 50px"
				class="mainbox col-md-5 col-md-offset-4 col-sm-7 col-sm-offset-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title text-center">
							Sign Up <i class="fa fa-user-plus"></i>
						</div>
						<div
							style="float: right; font-size: 85%; position: relative; top: -10px">
							<a id="signinlink" href="#"
								onclick="$('#signupbox').hide(); $('#loginbox').show()">Sign
								In</a>
						</div>
					</div>
					<div class="panel-body">
						<div id="signupalert" style="display: none"
							class="alert alert-danger">
							<p>Error:</p>
							<span></span>
						</div>
						<form:form action="save/user" method="post"
							class="form-horizontal" modelAttribute="forumUser"
							enctype="multipart/form-data">
							<div class="form-group">
								<label for="email" class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control input-sm" name="email"
										placeholder="Email Address">
								</div>
							</div>
							<div class="form-group">
								<label for="firstName" class="col-md-4 control-label">First
									Name</label>
								<div class="col-md-8">
									<input type="text" class="form-control input-sm"
										name="firstName" placeholder="First Name">
								</div>
							</div>
							<div class="form-group">
								<label for="lastName" class="col-md-4 control-label">Last
									Name</label>
								<div class="col-md-8">
									<input type="text" class="form-control input-sm"
										name="lastName" placeholder="Last Name">
								</div>
							</div>
							<div class="form-group">
								<label for="age" class="col-md-4 control-label">Age</label>
								<div class="col-md-8">
									<input type="number" class="form-control input-sm" name="age"
										placeholder="Age">
								</div>
							</div>
							<div class="form-group">
								<label for="password" class="col-md-4 control-label">Password</label>
								<div class="col-md-8">
									<input type="password" class="form-control input-sm"
										name="password" placeholder="Password" pattern="[a-zA-Z0-9-]+">
								</div>
							</div>
							<div class="form-group">
								<label for="genderDropDown" class="col-md-4 control-label">Gender</label>
								<div class="col-md-8">
									<select class="custom-select input-sm" id="genderDropDown"
										name="gender">
										<option selected>Choose...</option>
										<option value="1">Male</option>
										<option value="0">Female</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Roles</label>
								<div class="col-md-8">
									<div class="form-check-inline">
										<label class="form-check-label">
										  <input type="radio" class="form-check-input" name="authority" value="Developer">Developer
										</label>
									  </div>
									  <div class="form-check-inline">
										<label class="form-check-label">
										  <input type="radio" class="form-check-input" name="authority" value="Manager">Manager
										</label>
									  </div>
									  <div class="form-check-inline disabled">
										<label class="form-check-label">
										  <input type="radio" class="form-check-input" name="authority" value="Human Resources">Human Resources
										</label>
									  </div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Photo</label>
								<div class="custom-file col-md-8">
									<input type="file" class="custom-file-input" id="customFile"
										name="file" accept="image/x-png,image/gif,image/jpeg"></input>
									<label class="custom-file-label" for="customFile">Upload
										Photo</label>
								</div>
							</div>

							<div class="form-group">
								<!-- Button -->
								<div class="col-md-offset-4 col-md-8">
									<div class="col-sm-12 controls text-center">
										<input type="submit" class="btn btn-default btn-sm"
											value="Sign Up">
									</div>
								</div>
							</div>
						</form:form>
						<div class="form-group">
							<div class="col-md-12 control">
								<div
									style="border-top: 1px solid #888; padding-top: 15px; font-size: 85%">
									Do you have an account! <a href="#"
										onClick="$('#signupbox').hide(); $('#loginbox').show()">
										Login Here </a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</body>
</html>
