<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<title>ALEXNET</title>
<style>
.carousel-item {
	height: 500px;
}

/* CSS Test begin */
.comment-box {
	margin-top: 30px !important;
}

/* CSS Test end */
.comment-box img {
	width: 50px;
	height: 50px;
}

.comment-box .media-left {
	padding-right: 10px;
	width: 65px;
}

.comment-box .media-body p {
	border: 1px solid #ddd;
	padding: 10px;
}

.comment-box .media-body .media p {
	margin-bottom: 0;
}

.comment-box .media-heading {
	background-color: #f5f5f5;
	border: 1px solid #ddd;
	padding: 7px 10px;
	position: relative;
	margin-bottom: -1px;
}

.comment-box .media-heading:before {
	content: "";
	width: 12px;
	height: 12px;
	background-color: #f5f5f5;
	border: 1px solid #ddd;
	border-width: 1px 0 0 1px;
	-webkit-transform: rotate(-45deg);
	transform: rotate(-45deg);
	position: absolute;
	top: 10pxent left: -6px;
}

.carousel {
	overflow: hidden
}

#commentSection {
	left: 21px;
}

#commentDiv {
	left: -51px;
}

.carousel-item {
	height: 500px;
}

body {
	background-color: rgb(51, 53, 58);
}

.jumbotron {
	background-color: rgb(42, 44, 51);
}

#ideaBoxTitle, #ideaParagraph {
	color: #FFFFFF;
	text-shadow: 0 -1px 4px #FFF, 0 -2px 10px #ff0, 0 -10px 20px #ff8000, 0
		-18px 40px #F00;
	color: #FFFFFF;
}

#demoObject {
	-webkit-box-shadow: #FFF 0 -1px 4px, #ff0 0 -2px 10px, #ff8000 0 -10px
		20px, red 0 -18px 40px, 6px 7px 50px 22px #000000;
	box-shadow: #FFF 0 -1px 4px, #ff0 0 -2px 10px, #ff8000 0 -10px 20px, red
		0 -18px 40px, 6px 7px 50px 22px #000000;
	background: #ECEFCF;
}

body#tinymce {
	background: #FFFFFF
}

.commentSectionText {
	background: #333333;
	text-shadow: 0 -1px 4px #FFF, 0 -2px 10px #ff0, 0 -10px 20px #ff8000, 0
		-18px 40px #F00;
	color: #FFFFFF;
	background: #333333;
}

blockquote cite {
	color: #FFFFFF;
}

.blockquote-footer {
	color: #FFFFFF;
}

#submitCommentButton {
	background: #CECECE;
	background: -moz-linear-gradient(left, #CECECE 0%, #4F4D4F 50%, #000000 100%);
	background: -webkit-linear-gradient(left, #CECECE 0%, #4F4D4F 50%, #000000 100%);
	background: linear-gradient(to right, #CECECE 0%, #4F4D4F 50%, #000000 100%);
}

#jumbotronId h1 {
	text-shadow: 0 1px #808d93, -1px 0 #cdd2d5, -1px 2px #808d93, -2px 1px
		#cdd2d5, -2px 3px #808d93, -3px 2px #cdd2d5, -3px 4px #808d93, -4px
		3px #cdd2d5, -4px 5px #808d93, -5px 4px #cdd2d5, -5px 6px #808d93,
		-6px 5px #cdd2d5, -6px 7px #808d93, -7px 6px #cdd2d5, -7px 8px #808d93,
		-8px 7px #cdd2d5, -40px -40px 0px rgba(28, 110, 164, 0);
	color: #FFFFFF;
}

.divFixedSizeWidth {
	width: 1100px;
}
</style>
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<nav
					class="navbar fixed-top navbar-expand-lg navbar-light bg-light rounded">

					<button class="navbar-toggler" type="button" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="navbar-toggler-icon"></span>
					</button>
					<a class="navbar-brand" href="#">ALEXO</a>
					<div class="collapse navbar-collapse"
						id="bs-example-navbar-collapse-1">
						<ul class="navbar-nav">
							<li class="nav-item active"><a class="nav-link" href="#">Events
									<span class="sr-only">(current)</span>
							</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Schedule</a></li>
							<li class="nav-item"><a class="nav-link" href="people">People</a></li>
						</ul>
						<ul class="navbar-nav ml-md-auto">
							<li class="nav-item active"><a class="nav-link" href="#">
									${sessionScope.currentUser.firstName} <span class="sr-only">(current)</span>
							</a></li>
							<li><a id="modal-383649" href="#modal-container-383649"
								role="button" class="btn" data-toggle="modal">Logout</a>

								<div class="modal fade" id="modal-container-383649"
									role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
									data-backdrop="false">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="myModalLabel">Do you really
													want to log out?</h5>
												<button type="button" class="close" data-dismiss="modal">
													<span aria-hidden="true">�</span>
												</button>
											</div>
											<div class="modal-footer">
												<form:form action="/logout" method="get">
													<input type="submit" value="Yes" class="btn btn-danger" />
												</form:form>
												<button type="button" class="btn btn-secondary"
													data-dismiss="modal">Close</button>
											</div>
										</div>

									</div>

								</div>
					</div>
			</div>
			</li>
			</ul>
		</div>
		</nav>
		<div id="ht-tm-jumbotron">
			<div
				class="jumbotron bg-accent-2 text-white mb-0 radius-0 ht-tm-jumbotron mt-5"
				id="jumbotronId">
				<div class="container" id="dasdsaasd">
					<div class="ht-tm-header text-center" id="dasdsaasd">

						<h1 class="display-4 text-white">Organize your schedule!Get
							to know your coleagues and share some ideas amongst yourselves</h1>
						<div class="lead mb-3">ALEXNET is the new black,pun
							intended, in intercompany websites</div>

						<div class="mt-4">

							<a href="/bootstrap-themes/demo/files/hackerthemes-charming.zip"
								class="btn btn-outline-light btn-hover-text-accent-2 btn-lg my-3 mx-1 text-left js-ht-download-link"
								data-type="theme" data-id="88">People</a> <a
								href="/bootstrap-templates/charming-pro"
								class="btn btn-accent-1 btn-lg my-3 mx-1 text-left"> <span>Schedule</span>
							</a>
						</div>

					</div>
				</div>
			</div>
		</div>
		<center>
			<button type="button" class="btn btn-secondary mt-3"
				data-container="body" data-toggle="popover" data-placement="top"
				data-content="In the table below the three newest users have been listed!Make them feel at home :)">
				Say hi to the new users</button>
		</center>
		<table class="table table-hover table-dark mt-1">
			<thead>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Age</th>
					<th>Email</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${newestUsers}" var="newUser">
					<tr>
						<td>${newUser.firstName}</td>
						<td>${newUser.lastName}</td>
						<td>${newUser.age}</td>
						<td>${newUser.email}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<p class="text-center" id="ideaParagraph">
					In the section below feel free to submit you ideas in order for us
					to better our website. Every submission will be anonymous so don't
					worry <br> <a id="modal-761057" href="#modal-container-761057"
						role="button" class="btn" data-toggle="modal">New Idea</a>
				<div class="modal fade" id="modal-container-761057" role="dialog"
					aria-labelledby="myModalLabel" aria-hidden="true"
					data-backdrop="false">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="myModalLabel">Submit your
									awesome idea!!</h5>
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">�</span>
								</button>
							</div>
							<div class="modal-body">
								<center>
									<form:form id="saveTopic" class="form-horizontal"
										modelAttribute="newIdea" action="/idea" method="post">
										<div class="form-group">
											<label for="topicTitle"> Title </label> <input type="text"
												name="title" class="form-control" id="topicTitle" />
										</div>
										<div class="form-group">
											<label for="topicDescription"> Description </label>
											<textarea class="form-control" name="description" rows="5"
												id="comment"></textarea>
										</div>
										<input type="submit" class="btn btn-primary btn-sm"
											value="Save">
										<input type="hidden" name="id" value="${currentUser.id}" />
									</form:form>
								</center>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary"
									data-dismiss="modal">Close</button>
							</div>
						</div>

					</div>

				</div>
				</p>
			</div>
			<div class="col-md-2"></div>

		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<div class="row ">
							<div class="col-md-12 rounded p-3 m-3 " id="ideaDiv">
								<h3 class="text-center text-muted mb-5">
									<div id="ideaBoxTitle">The Idea Box</div>
								</h3>
								<div class="container ">
									<div class="row ">
										<center>
											<c:forEach items="${ideaTopics}" var="currentIdeaTopic">
												<div
													class="col-md-12 rounded bg-dark p-3 mt-3 commentSectionText divFixedSizeWidth"
													id="demoObject">
													<div class="col-md-12 ">
														<div class="col-md-10">
															<p>
															<h2>
																<i class="fas fa-lightbulb"></i>
															</h2>
															</p>
															<p>
															<blockquote class="blockquote">
																<p class="mb-0">
																<h3>${currentIdeaTopic.title}</h3>
																${currentIdeaTopic.description}
																</p>
																<footer class="blockquote-footer">
																	${currentIdeaTopic.user.firstName}
																	${currentIdeaTopic.user.lastName} <cite>-
																		published on: ${currentIdeaTopic.publishDate}</cite>
																</footer>
															</blockquote>
															<c:forEach items="${currentIdeaTopic.comments}"
																var="currentComment">
																<div class="row">
																	<div class="col-md-2"></div>
																	<div class="col-md-8">
																		<div class="media mt-2 float-left">
																			<img class="mr-3 rounded-circle"
																				alt="Bootstrap Media Preview"
																				src="getEmployeePhoto/${currentComment.user.id}"
																				style="width: 5rem; height: 5rem;" />
																			<blockquote class="blockquote blockquote-reverse">
																				<p class="mb-0">${currentComment.comment}</p>
																				<footer class="blockquote-footer">
																					${currentComment.user.firstName}
																					${currentComment.user.lastName} <cite>-
																						published on: ${currentComment.publishedOn}</cite>
																				</footer>
																			</blockquote>

																		</div>
																	</div>
																	<div class="col-md-2"></div>
																</div>

															</c:forEach>

														</div>

													</div>
													<section class="comments-outer mt-1 p-3">
														<div class="card my-4 bg-dark border border-white">
															<h5 class="card-header">Leave a Comment:</h5>
															<div class="card-body border border-white"
																id="#borderDemo">
																<form:form action="/postComment" method="post"
																	modelAttribute="newComment">
																	<div class="form-group">
																		<textarea rows="3" class="form-control bg-dark"
																			name="comment" style="color: #FFFFFF"></textarea>
																		<input type="hidden" name="topicId"
																			value="${currentIdeaTopic.id}" /> <input
																			type="hidden" name="userId" value="${currentUser.id}" />
																	</div>
																	<button type="submit" id="submitCommentButton"
																		class="btn btn-dark">
																		<i class="fas fa-check-circle"></i>
																	</button>
																</form:form>
															</div>
														</div>
													</section>
												</div>
											</c:forEach>
										</center>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 rounded">
			<div
				class="alert alert-dismissible fade show alert-dark col-md-4 offset-md-4"
				role="alert">
				<button type="button" class="close" data-dismiss="alert"
					aria-hidden="true">�</button>
				<h4>Alert!</h4>
				<strong>Hey!</strong> Look at these awesome news!Why not read a
				couple!
			</div>
			<div class="row">
				<div class="col-md-4 rounded">
					<div class="card rounded">
						<img class="card-img-top" alt="Bootstrap Thumbnail First"
							src="https://www.layoutit.com/img/people-q-c-600-200-1.jpg" />
						<div class="card-block rounded">
							<h5 class="card-title">Find your inner peace</h5>
							<p class="card-text">Indulge in yoga training with you
								colleagues every friday after work. This is a new campaign that
								has been started in order to build the team.</p>
							<p>
								</a> <a class="btn" href="#">Quick Read</a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 rounded">
					<div class="card rounded">
						<img class="card-img-top" alt="Bootstrap Thumbnail Second"
							src="https://www.layoutit.com/img/city-q-c-600-200-1.jpg" />
						<div class="card-block rounded">
							<h5 class="card-title">Teambuilding coming up</h5>
							<p class="card-text">Next may the company has planned to take
								you the beloved employees to a trip all over europe! So you
								better start packing!</p>
							<p>
								</a> <a class="btn" href="#">Quick Read</a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 rounded">
					<div class="card rounded">
						<img class="card-img-top" alt="Bootstrap Thumbnail Third"
							src="https://www.layoutit.com/img/sports-q-c-600-200-1.jpg" />
						<div class="card-block rounded">
							<h5 class="card-title">Why you need to do sports?</h5>
							<p class="card-text">We appreciate your hard work,but take a
								look at this article and educate yourself why a bit of spare
								time and sports can improve your everyday life!</p>
							<p>
								</a> <a class="btn" href="#">Quick Read</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
	<script>
		$(document).ready(function() {
			$("[data-toggle=popover]").popover();
			$("#replyButton").click(function() {
				$('#commentDiv').attr('class', 'col-md-6 offset-md-10');
			});
			$("#cancelCommnet").click(function() {
				$('#commentDiv').attr('class', 'col-md-6 offset-md-10 d-none');
			});
		});
	</script>
</body>

</html>