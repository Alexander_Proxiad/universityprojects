package quality.code.uni.project.model;

/**
 * Entity like class used to store the parameters that are needed to create a
 * new topic
 * 
 * @author a.vulchev
 *
 */
public class IdeaBean {

	private String title;
	private String description;
	private int id;

	/**
	 * 
	 */
	public IdeaBean() {
		super();
	}

	/**
	 * @param title
	 * @param description
	 * @param id
	 */
	public IdeaBean(String title, String description, int id) {
		super();
		this.title = title;
		this.description = description;
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SaveTopicBean [title=" + title + ", description=" + description + ", id=" + id + "]";
	}

}
