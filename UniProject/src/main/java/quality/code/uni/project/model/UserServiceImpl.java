package quality.code.uni.project.model;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Autowired
	private RoleDao roleDao;

	@Override
	public User extractUserFromDatabase(LoginBean loginUser) {
		return userDao.extractUserFromDatabase(loginUser);
	}

	@Override
	public User getUser(int id) {
		return userDao.get(id);
	}

	@Override
	public List<User> getThreeNewestUsers() {
		return userDao.getThreeNewestUsers();
	}

	@Override
	public List<User> getAllUsers() {
		return userDao.getAll();
	}

	@Override
	public void saveUserWithBlob(User user, MultipartFile photo) {
		user.setActive(1);
		user.setJoinedOn(new Date());
		user.setPassword(encoder.encode(user.getPassword()));
		if (user.getAuthority().equalsIgnoreCase("Developer")) {
			user.setRoles(roleDao.getDeveloperRoles());
		} else if (user.getAuthority().equalsIgnoreCase("Manager")) {
			user.setRoles(roleDao.getManagerRoles());
		} else if (user.getAuthority().equalsIgnoreCase("Human Resources")) {
			user.setRoles(roleDao.getHumanResourcesRoles());
		}
		userDao.saveUserWithBlob(user, photo);

	}

	@Override
	public void extractEmployeePicture(HttpServletResponse response, int id) throws SQLException, IOException {
		response.setContentType("image/jpeg");

		User user = userDao.get(id);
		Blob blob = user.getPhoto();
		byte[] bytes = blob.getBytes(1, (int) blob.length());
		InputStream inputStream = new ByteArrayInputStream(bytes);
		IOUtils.copy(inputStream, response.getOutputStream());
	}

}
