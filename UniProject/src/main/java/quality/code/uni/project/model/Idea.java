package quality.code.uni.project.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Topic entity consisting of the following set of parameters: The id of the
 * topic,title,description,publishDate which are stored in the FORUM_TOPIC table
 * in the database;
 * 
 * @author a.vulchev
 *
 */
@Entity
@Table(name = "FORUM_IDEA")
public class Idea {

	@Id
	@Column(name = "idea_id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "title", nullable = false, length = 100)
	private String title;

	@Column(name = "description", nullable = false, length = 1000)
	private String description;

	@Column(name = "publish_date", nullable = false)
	private Date publishDate;

	@OneToMany(mappedBy = "idea", fetch = FetchType.EAGER)
	private List<Comment> comments;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private User user;

	public Idea() {
		super();
	}

	public Idea(String title, String description, Date publishDate, List<Comment> comments, User user) {
		super();
		this.title = title;
		this.description = description;
		this.publishDate = publishDate;
		this.comments = comments;
		this.user = user;
	}

	/**
	 * Constructor with all the fields available in the user Entity
	 * 
	 * @param id
	 * @param title
	 * @param description
	 * @param publishDate
	 * @param comments
	 * @param user
	 */
	public Idea(int id, String title, String description, Date publishDate, List<Comment> comments, User user) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.publishDate = publishDate;
		this.comments = comments;
		this.user = user;
	}

	public String getDescription() {
		return description;
	}

	public int getId() {
		return id;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public String getTitle() {
		return title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Topic [id=" + id + ", title=" + title + ", description=" + description + ", publishDate=" + publishDate
				+ "]";
	}

}
