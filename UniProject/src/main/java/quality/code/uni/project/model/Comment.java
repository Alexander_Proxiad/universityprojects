package quality.code.uni.project.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Comment entity consisting of the following set of parameters: The id of the
 * comment,comment,publishedOn which are stored in the idea_COMMENT table in the
 * database;
 * 
 * @author a.vulchev
 *
 */
@Entity
@Table(name = "idea_COMMENT")
public class Comment {

	@Id
	@Column(name = "comment_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "comment", nullable = false, length = 255)
	private String comment;

	@Column(name = "published_on")
	private Date publishedOn;

	@ManyToOne
	@JoinColumn(name = "idea_id")
	private Idea idea;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	/**
	 * Constructor for class "Comment" with the following parameters. Id is excluded
	 * from those parameters because it is set to auto increment and there is no
	 * need for manual initialization
	 * 
	 * @param comment
	 * @param publishedOn
	 * @param idea
	 * @param user
	 */
	public Comment(String comment, Date publishedOn, Idea idea, User user) {
		super();
		this.comment = comment;
		this.publishedOn = publishedOn;
		this.idea = idea;
		this.user = user;
	}

	/**
	 * No-args constructor for class "Comment"
	 */
	public Comment() {
		super();
	}

	public String getComment() {
		return comment;
	}

	public int getId() {
		return id;
	}

	public Date getPublishedOn() {
		return publishedOn;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPublishedOn(Date publishedOn) {
		this.publishedOn = publishedOn;
	}

	public Idea getidea() {
		return idea;
	}

	public void setidea(Idea idea) {
		this.idea = idea;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Comment [id=" + id + ", comment=" + comment + ", publishedOn=" + publishedOn + "]";
	}

}
