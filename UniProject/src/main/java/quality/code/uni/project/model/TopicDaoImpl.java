package quality.code.uni.project.model;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

@Repository
public class TopicDaoImpl extends GenericDaoImpl<Idea> implements TopicDao {

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<Idea> getTopics() {
		return getSession().createCriteria(Idea.class, "topic").setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<Idea> getIdeasAscending() {
		return getSession().createCriteria(Idea.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.addOrder(Order.asc("id")).list();
	}

}
