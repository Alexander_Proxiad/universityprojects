package quality.code.uni.project.model;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public interface UserDao extends GenericDao<User> {

	/**
	 * Method getting user from database based on a specific id
	 * 
	 * @param loginUser
	 * @return
	 */
	User extractUserFromDatabase(LoginBean loginUser);

	/**
	 * Method that creates a criteria returning the 3 newest registered users from
	 * the database
	 * 
	 * @return
	 */
	List<User> getThreeNewestUsers();

	/**
	 * Method saving a user with blob to the database
	 * 
	 * @param user  User
	 * @param photo MultipartFile
	 */
	void saveUserWithBlob(User user, MultipartFile photo);

}
