package quality.code.uni.project.model;

import java.util.List;

public interface TopicDao extends GenericDao<Idea> {

	/**
	 * Method gets a list of topics from the database
	 * 
	 * @return
	 */
	List<Idea> getTopics();

	List<Idea> getIdeasAscending();

}
