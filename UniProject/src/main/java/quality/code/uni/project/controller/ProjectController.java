package quality.code.uni.project.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import quality.code.uni.project.model.Idea;
import quality.code.uni.project.model.IdeaBean;
import quality.code.uni.project.model.LoginBean;
import quality.code.uni.project.model.NewCommentBean;
import quality.code.uni.project.model.TopicService;
import quality.code.uni.project.model.User;
import quality.code.uni.project.model.UserService;

/**
 * Controller for all the C.R.U.D operations that occur
 * 
 * @author a.vulchev
 *
 */
@Controller
public class ProjectController {

	@Autowired
	private TopicService topicService;

	@Autowired
	private UserService userService;

	/**
	 * Displays the home page and simulating a logged in experience via sessionScope
	 * 
	 * @param theModel
	 * @param request
	 * @param response
	 * @return
	 */
	@GetMapping("/login")
	public String homePage(Model theModel, HttpServletRequest request, HttpServletResponse response) {

		User user = new User();
		LoginBean loginBean = new LoginBean();
		theModel.addAttribute("forumUser", user);
		theModel.addAttribute("userLogin", loginBean);
		return "signIn";
	}

	/**
	 * Saves the user to the database table that is mapped for the user. Return type
	 * String which is passed into the view resolver that returns a jsp page to be
	 * displayed
	 * 
	 * @param user
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	@PostMapping("/save/user")
	public String saveUser(@ModelAttribute(name = "forumUser") User user, @RequestParam("file") MultipartFile file)
			throws IOException {
		try {
			userService.saveUserWithBlob(user, file);
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:/unsuccesfullRegistration";
		}
		return "redirect:/success";

	}

	/**
	 * Helper controller method to prevent form resubmission after registration
	 * 
	 * @return
	 */
	@GetMapping("/success")
	public String helperSuccess() {
		return "success";
	}

	/**
	 * Controller that stores loads the data that is connected to the user that was
	 * logged in from the jsp.The data is stored in the session and then displayed
	 * in the next jsp page
	 * 
	 * @param loginUser
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@GetMapping("/home")
	public void logInForum(@ModelAttribute("userLogin") LoginBean loginUser, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		loginUser.setEmail(userDetails.getUsername());
		loginUser.setPassword(userDetails.getPassword());
//		String email = (String) request.getSession().getAttribute("email");
		User loggedInUser = userService.extractUserFromDatabase(loginUser);
		List<Idea> ideaTopics = topicService.getTopics();
		List<User> newestUsers = userService.getThreeNewestUsers();
		HttpSession session = request.getSession();
		if (loggedInUser != null) {
			session.setAttribute("currentUser", loggedInUser);
			session.setAttribute("ideaTopics", ideaTopics);
			session.setAttribute("newestUsers", newestUsers);
			response.sendRedirect("main-page");
		} else {
			response.sendRedirect("unsuccesfulLogin");
		}

	}

	/**
	 * Helper method to prevent form resubmission and redirect to the main page
	 * 
	 * @return
	 */
	@GetMapping("/main-page")
	public String redirectToMain() {
		return "main-page";
	}

	/**
	 * Helper method that redirects to error page whenever false login is entered
	 * 
	 * @return
	 */
	@GetMapping("/unsuccesfulLogin")
	public String redirectToErrorPageOnLogin() {
		return "unsuccesfulLogin";
	}

	/**
	 * Helper method that redirects to error page whenever false login is entered
	 * 
	 * @return
	 */
	@GetMapping("/unsuccesfullRegistration")
	public String redirectToErrorPageOnRegistration() {
		return "unsuccesfullRegistration";
	}

	/**
	 * Kills the session in order for another user to log in returns to the login
	 * form jsp
	 * 
	 * @param session
	 * @return
	 */
	@GetMapping("/logout")
	public String getBackToLoginPage(HttpSession session) {
		session.invalidate();
		return "redirect:/login";
	}

	/**
	 * Saves a topic with the parameters from the model attribute to the
	 * database.Redirects to the helper method for home page
	 * 
	 * @param ideaBean
	 * @return
	 */
	@PostMapping("/idea")
	public String saveIdea(@ModelAttribute("newIdea") IdeaBean ideaBean, HttpServletRequest request) {
		try {
			List<Idea> ideaTopics = topicService.saveIdeaAndSetAllIdeas(ideaBean);
			HttpSession session = request.getSession();
			session.setAttribute("ideaTopics", ideaTopics);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/main-page";
	}

	/**
	 * Method posting a comment to a specific theme from a specific user all of
	 * which is later being displayed in the UI
	 * 
	 * @param commentBean
	 * @param request
	 * @return String redirecting to helper method that displays the main page
	 */
	@PostMapping("/postComment")
	public String saveCommentToTopic(@ModelAttribute("newIdea") NewCommentBean commentBean,
			HttpServletRequest request) {
		try {
			List<Idea> ideaTopics = topicService.saveCommentToTopicAndSetAllTopics(commentBean);
			HttpSession session = request.getSession();
			session.setAttribute("ideaTopics", ideaTopics);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/main-page";
	}

	/**
	 * Method retrieving a list of user from the database
	 * 
	 * @return String value redirecting to a jsp
	 * @throws IOException
	 */
	@GetMapping(value = "/people")
	public String listEmployees(HttpServletRequest request, HttpSession session) throws IOException {

		List<User> allEmployees = userService.getAllUsers();
		session = request.getSession();
		session.setAttribute("AllEmployees", allEmployees);

		return "peopleList";
	}

	/**
	 * Method extracting photo based on id and setting it in the ui
	 * 
	 * @param response HttpServletResponse
	 * @param id       int
	 * @throws Exception
	 */
	@RequestMapping(value = "/getEmployeePhoto/{id}")
	public void getEmployeePhoto(HttpServletResponse response, @PathVariable("id") int id) throws Exception {
		userService.extractEmployeePicture(response, id);
	}

}
