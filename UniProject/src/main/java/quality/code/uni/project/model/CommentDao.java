package quality.code.uni.project.model;

import java.util.List;

public interface CommentDao extends GenericDao<Comment> {

	/**
	 * Method extracting all comments for perticular idea
	 * 
	 * @param id - integer value of Idea identifier
	 * @return List<Comment>
	 */
	List<Comment> getCommentsForIdeaWithIdeaId(int id);

}
