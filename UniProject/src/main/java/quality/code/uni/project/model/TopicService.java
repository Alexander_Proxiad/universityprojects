package quality.code.uni.project.model;

import java.util.List;

public interface TopicService {

	/**
	 * Method that gets a specific topic from the database based on id
	 * 
	 * @param id
	 * @return
	 */
	List<Idea> getTopics();

	/**
	 * Method saving comment on certain idea to the database and setting all the
	 * existing ideas as a session attribute
	 * 
	 * @param commentBean NewCommentBean
	 * @return List<Idea>
	 */
	List<Idea> saveCommentToTopicAndSetAllTopics(NewCommentBean commentBean);

	/**
	 * Method saving idea to the database and setting all the existing ideas as a
	 * session attribute
	 * 
	 * @param ideaBean IdeaBean
	 * @return List<Idea>
	 */
	List<Idea> saveIdeaAndSetAllIdeas(IdeaBean ideaBean);

}
